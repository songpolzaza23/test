import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class HomeService {

  noAuthHeader = { headers: new HttpHeaders({ 'noauth' : 'True'}) };

  constructor(private http : HttpClient) { }

  getData(username) {
    return this.http.post(environment.apiBaseUrl + '/getUser' , username , this.noAuthHeader)
  }

  uploadImg(data) {
    console.log(data)
    return this.http.post(environment.apiBaseUrl + '/uploadImg' , data , this.noAuthHeader) 
  }

} 
