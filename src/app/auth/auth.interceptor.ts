import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Router } from "@angular/router";
import { from, observable } from 'rxjs';

@Injectable()

export class AuthInterceptor implements HttpInterceptor {

    constructor(private router : Router){}

     intercept( req: HttpRequest<any>, next: HttpHandler) {
         
        if (req.headers.get('noauth'))  {
            const clonedreq = req.clone({
                headers: req.headers.set("Authorization", "Bearer " + localStorage.getItem('token'))
            });
            return next.handle(clonedreq).pipe(
                tap(
                    event => { },
                    err => {
                        if (err.error.auth == false) {
                            this.router.navigateByUrl('/login');
                        }
                    })
            );
        }
    }

}



            //     let promise = this.storage.get('token');
    //     console.log(promise)

    //     return observable.toString()
    //         .mergeMap(token => {
    //             let clonedReq = this.addToken(req, token);
    //             return next.handle(clonedReq);
    //         }); 
    // }

    // private addToken(request: HttpRequest<any>, token: any) {
    //     if (token) {
    //         let clone: HttpRequest<any>;
    //         clone = request.clone({
    //             setHeaders: {
    //                 Accept: 'application/json',
    //                 'Content-Type': 'application/json',
    //                 Authorization: "Bearer " + token
    //             }
    //         });
    //         return clone;
    //     }
    // }