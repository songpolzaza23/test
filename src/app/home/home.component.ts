import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { UserService } from '../shared/user/user.service';
import { HomeService } from '../shared/home/home.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private router:Router, private homeService : HomeService ,private http: HttpClient) {}


  model = {
    username: '59161081'
  }

  img = {
    images: ''
  }

  ngOnInit() {
    this.getdata(this.model)
  }

  imageUrl : string = "";
  fileToUpload : File = null
  fd = new FormData();

  handleFileInput(file: FileList){
    this.fileToUpload = file.item(0);
    this.fd.append('file', this.fileToUpload, this.model.username+'.'+this.fileToUpload.name);
    console.log(this.imageUrl)

    //show image preview
    var reader = new FileReader();
    reader.onload = (event:any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload)
    this.homeService.uploadImg(this.fd).subscribe(
      (res) => {
        console.log(res)
      },
      err => {
        console.log(err)
      }
    )
  }


  getdata(model) {
    this.homeService.getData(model).subscribe(
      (res) => {
        this.imageUrl = 'http://thebeach.buu.in.th:4011/images/'+res['img'];
        console.log(this.imageUrl )
        console.log(res)
        //console.log(localStorage.getItem('token'))
        // this.userService.deleteToken();
      },
      err => {
        console.log(err)
        // this.router.navigateByUrl('/login');
      }
    )
  }

  onLogout() {
    // this.userService.deleteToken();
    this.router.navigate(['/login']);
  }
}